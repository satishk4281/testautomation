package com.automationpractice.utilities;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.chrome.*;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.WebElement;

public class BaseTest {

	public static WebDriver driver;
	public static WebDriver openBrowser() {

		System.setProperty("webdriver.chrome.driver", "E:\\Drivers\\chromedriver.exe");   

		driver=new ChromeDriver();
		driver.get("http://automationpractice.com/index.php?");
		driver.manage().window().maximize();
		return driver;

	}

	public static WebElement findElement(String loctor,String path) {
		WebElement we = null;
		if(loctor.equalsIgnoreCase("xpath"))
			we=driver.findElement(By.xpath(path));
		else if(loctor.equalsIgnoreCase("id"))
			we= driver.findElement(By.id(path));
		return we;
	}

	public static List<WebElement> findElements(String loctor,String path) {
		if(loctor.equalsIgnoreCase("xpath"))
			return driver.findElements(By.xpath(path));
		else if(loctor.equalsIgnoreCase("id"))
			return driver.findElements(By.id(path));
		else
			return null;
	}
	public static void click(WebElement element) {

		element.click();

	}
	
	public static void clickWithJs(WebElement element) {
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
	}

	public static void closeBrowser() {
		driver.quit();	
	}
	public static String getAttribute(WebElement element,String key) {

		return element.getAttribute(key);

	}

	public static String getText(WebElement element) {

		return element.getText();

	}

	public static String getSelectedOption(WebElement element) {
		Select s=new Select(element);
		return getText(s.getFirstSelectedOption());

	}

	public static boolean isElementPresent(WebElement element) {
		return element.isDisplayed();
	}

	public static void waitInSeconds(int Seconds) {
		try {
			Thread.sleep(Seconds);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	public static void type(WebElement element, String value) {

		element.sendKeys(value);

	}

	public static  void waitForLoad() {
		driver.manage().timeouts().implicitlyWait(1000, TimeUnit.SECONDS);
	}
	
	public static int getRandomIntegerFrom0ToMaxRange(int max){
		Random rnd = new Random();
		if(max!=0)		
			return rnd.nextInt(max); 
		else 
			return 0;
	}
	
}
