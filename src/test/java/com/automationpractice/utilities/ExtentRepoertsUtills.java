package com.automationpractice.utilities;

import java.net.UnknownHostException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class ExtentRepoertsUtills {
	static ExtentHtmlReporter ehr;
	static ExtentReports er;
	static ExtentTest et;

	public static void initReports() throws UnknownHostException{
		ehr = new ExtentHtmlReporter(System.getProperty("user.dir") +"/test-output/MyOwnReport.html");
		er = new ExtentReports();
		er.attachReporter(ehr);

		er.setSystemInfo("Host Name", java.net.InetAddress.getLocalHost().getHostName());
		er.setSystemInfo("Environment", "Automation Testing");
		er.setSystemInfo("User Name", System.getProperty("user.name"));
		er.setSystemInfo("OS", System.getProperty("os.name"));

		ehr.config().setDocumentTitle("Extent Reports");
		ehr.config().setReportName(" Automation Report");
		ehr.config().setTheme(Theme.DARK);
		ehr.config().setChartVisibilityOnOpen(true);
		ehr.config().setTestViewChartLocation(ChartLocation.TOP);

	}

	public static void MyExtentReportsTestCase(String testCaseName) {
		et = er.createTest(testCaseName);
	}

	public static void MyExtentReportsStatus(String stepName,String details,String status) {
		try{
			et.log(Status.valueOf(status.toUpperCase()), details);
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}

	public static void MyExtentRepotsFlush() {
		er.flush();
	}
}
