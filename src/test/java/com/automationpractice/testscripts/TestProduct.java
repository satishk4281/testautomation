package com.automationpractice.testscripts;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

//import java.util.concurrent.TimeUnit;

//import org.openqa.selenium.WebDriver;
//import org.testng.Assert;
import org.testng.annotations.Test;

import com.automationpractice.pageobject.HomePage;
import com.automationpractice.pageobject.ProductDescriptionPage;
import com.automationpractice.pageobject.SearchPage;
import com.automationpractice.pageobject.ShopingCartSummaryPage;
import com.automationpractice.utilities.BaseTest;
import com.automationpractice.utilities.ExtentRepoertsUtills;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class TestProduct {

	public static ExtentHtmlReporter htmlReporter;
	public static ExtentReports extent;
	public static ExtentTest test;



	@Test public void validateSearchFunctinality() { 
		ExtentRepoertsUtills.MyExtentReportsTestCase("Search Functionality");
		BaseTest.openBrowser();
		HomePage home = new HomePage(); 
		String searchString="Dress";
		home.typeInSearchBox(searchString); 
		home.clickSearchButton(); 
		SearchPage search = new SearchPage();
		search.verifyResults(searchString);
		BaseTest.closeBrowser(); 
	}




	@Test public void validateSummary() {
		ExtentRepoertsUtills.MyExtentReportsTestCase("Adding Product to cart");
		BaseTest.openBrowser();
		List<String> existingProductDetails=new ArrayList<String>();
		HomePage home = new HomePage(); 
		String	searchString="Dress"; 
		home.typeInSearchBox(searchString);
		home.clickSearchButton(); 
		SearchPage search = new SearchPage();
		search.verifyResults(searchString); 
		if(!search.getResultsCount().equals("0")) {			
			List<String> productDetails=search.selectMulticolouredItem(existingProductDetails);
			ProductDescriptionPage pdp=	new ProductDescriptionPage(); 
			pdp.VerifyProdcutName(productDetails.get(0));
			pdp.VerifyColour(productDetails.get(1)); 
			productDetails.add(pdp.getSize());
			productDetails.add(pdp.getQty()); 
			pdp.clickAddtoCart();
			BaseTest.waitInSeconds(5000); 
			pdp.VerifyShoppingCart();
			pdp.VerifyProdcutDetailsInShoppingCart(productDetails);
			pdp.clickProceedToCheckout(); 
			ShopingCartSummaryPage scsp= new ShopingCartSummaryPage();
			scsp.VerifyProdcutDetailsInShoppingCartSummary(productDetails); }

		BaseTest.closeBrowser(); }



	@Test public void validateSummary2() {
		ExtentRepoertsUtills.MyExtentReportsTestCase("Adding Multiple Products to Cart");

		BaseTest.openBrowser(); 
		List<String> existingProductDetails=new ArrayList<String>();
		for (int i = 0; i < 3; i++) { 
			HomePage home = new HomePage(); 
			String searchString="Dress"; 
			home.typeInSearchBox(searchString);
			home.clickSearchButton(); 
			SearchPage search = new SearchPage();
			search.verifyResults(searchString); 
			if (!search.getResultsCount().equals("0")) { 
				List<String> productDetails=search.selectMulticolouredItem(existingProductDetails);
				existingProductDetails.add(productDetails.get(0)+"-"+productDetails.get(1));
				ProductDescriptionPage pdp=  new ProductDescriptionPage();
				pdp.VerifyProdcutName(productDetails.get(0));
				pdp.VerifyColour(productDetails.get(1));
				productDetails.add(pdp.getSize());
				productDetails.add(pdp.getQty()); 
				pdp.clickAddtoCart();
				BaseTest.waitInSeconds(5000); 
				pdp.VerifyShoppingCart();
				pdp.VerifyProdcutDetailsInShoppingCart(productDetails);
				pdp.clickProceedToCheckout(); 
				ShopingCartSummaryPage scsp= new ShopingCartSummaryPage();
				scsp.VerifyProdcutDetailsInShoppingCartSummary(productDetails); } }
		BaseTest.closeBrowser();
	}

	@BeforeSuite
	public void initReport() {
		try {
			ExtentRepoertsUtills.initReports();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}

	@AfterMethod
	public void getResult(ITestResult result){
		ExtentRepoertsUtills.MyExtentRepotsFlush();
	}
}
