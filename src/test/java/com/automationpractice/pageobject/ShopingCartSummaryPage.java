package com.automationpractice.pageobject;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.automationpractice.testscripts.TestProduct;
import com.automationpractice.utilities.BaseTest;
import com.automationpractice.utilities.ExtentRepoertsUtills;

public class ShopingCartSummaryPage {
	
	public  List<String> getProductNames() {
		List<String> productNames= new ArrayList<String>();
		
		List<WebElement> PNames=BaseTest.findElements("xpath", "//table[@id='cart_summary']/tbody//td//p[@class='product-name']/a");
		for (WebElement element : PNames) {
			productNames.add(BaseTest.getText(element));
		}
		return productNames;
	}
	
	public  List<String> getProductColours() {
		List<String> productColours= new ArrayList<String>();
		
		List<WebElement> PColours=BaseTest.findElements("xpath", "//table[@id='cart_summary']/tbody//td//small/a");
		for (WebElement element : PColours) {
			productColours.add(BaseTest.getText(element).split(",")[0].split(":")[1].toLowerCase().trim());
		}
		return productColours;
	}
	
	public  List<String> getProductSizes() {
		List<String> productSizes= new ArrayList<String>();
		
		List<WebElement> PSizes=BaseTest.findElements("xpath", "//table[@id='cart_summary']/tbody//td//small/a");
		for (WebElement element : PSizes) {
			productSizes.add(BaseTest.getText(element).split(",")[1].split(":")[1].trim());
		}
		return productSizes;
	}
	
	public  List<String> getProductQtys() {
		List<String> productQtys= new ArrayList<String>();
		
		List<WebElement> PQtys=BaseTest.findElements("xpath", "//table[@id='cart_summary']/tbody//td//input[@class='cart_quantity_input form-control grey']");
		for (WebElement element : PQtys) {
			productQtys.add(BaseTest.getAttribute(element, "value"));
		}
		return productQtys;
	}
	
	public  List<String> getProductTotals() {
		List<String> productTotals= new ArrayList<String>();
		
		List<WebElement> PTotals=BaseTest.findElements("xpath", "//table[@id='cart_summary']/tbody//td[@class='cart_total']//span[@class='price']");
		for (WebElement element : PTotals) {
			productTotals.add(BaseTest.getText(element).substring(1).trim());
		}
		return productTotals;
	}
	

	public  void VerifyProdcutDetailsInShoppingCartSummary(List<String> productDetails) {
		List<String> ProductNames=getProductNames();
		List<String> ProductColours=getProductColours();
		List<String> ProductSizes=getProductSizes();
		List<String> ProductQtys=getProductQtys();
		List<String> ProductTotals=getProductTotals();
		System.out.println("My Products totals :"+ProductTotals);
		boolean flag = false;
		for (int i = 0; i < ProductNames.size(); i++) {
			if(ProductNames.get(i).equals(productDetails.get(0)) &&
					ProductColours.get(i).equals(productDetails.get(1))&&
					ProductSizes.get(i).equals(productDetails.get(2))&&
					ProductQtys.get(i).equals(productDetails.get(3))) {
				flag=true;
				break;
			}				
		}
		ExtentRepoertsUtills.MyExtentReportsStatus("stepDescription","ProductDetails", "Pass");
		
		Assert.assertTrue(flag);
		}
}
