package com.automationpractice.pageobject;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.automationpractice.utilities.BaseTest;

public class HomePage {

	WebDriver driver1;
		
public void typeInSearchBox(String Item) {
	WebElement search =BaseTest.findElement("xpath", "//input[@class='search_query form-control ac_input']");
	
	BaseTest.type(search, Item);

}

public void clickSearchButton() {
	WebElement click =BaseTest.findElement("xpath", "//button[@name='submit_search']");
	
	BaseTest.click(click);

}

}


