package com.automationpractice.pageobject;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.automationpractice.utilities.BaseTest;
import com.automationpractice.utilities.ExtentRepoertsUtills;

public class ProductDescriptionPage {

	public  String getProductName() {
		return BaseTest.getText(BaseTest.findElement("xpath", "//h1[@itemprop='name']"));
	}
	
	public  void VerifyProdcutName(String name) {
		Assert.assertEquals(getProductName(), name);
	}
	
	public  String getSelectedColour() {
		return BaseTest.getAttribute(BaseTest.findElement("xpath", "//div[@class=\"attribute_list\"]/ul/li[@class='selected']/a"),"name").toLowerCase();
	}
	
	public  void VerifyColour(String colourName) {
		ExtentRepoertsUtills.MyExtentReportsStatus("colourstepDescription","colouractualResult", "Pass");
		Assert.assertEquals(getSelectedColour(), colourName);
	}
	
	public  String getQty() {
		return BaseTest.getAttribute(BaseTest.findElement("xpath", "//input[@id='quantity_wanted']"),"value");
	}
	
	public  String getSize() {
		return BaseTest.getSelectedOption(BaseTest.findElement("xpath", "//select[@id='group_1']"));
	}
	
	public  void clickAddtoCart() {
		BaseTest.click(BaseTest.findElement("xpath", "//p[@id='add_to_cart']"));
	}
	
	public  void VerifyShoppingCart() {
		Assert.assertEquals(BaseTest.isElementPresent(BaseTest.findElement("xpath", "//*[@id='layer_cart']/div[@class='clearfix']")), true);
	}
	
	public  void VerifyProdcutDetailsInShoppingCart(List<String> productDetails) {
		Assert.assertEquals(BaseTest.getText(BaseTest.findElement("xpath", "//*[@id=\"layer_cart\"]/div[@class='clearfix']//span[@id='layer_cart_product_title']")), productDetails.get(0));
		Assert.assertEquals(BaseTest.getText(BaseTest.findElement("xpath", "//*[@id=\"layer_cart\"]/div[@class='clearfix']//span[@id='layer_cart_product_attributes']")).split(",")[0].trim().toLowerCase(), productDetails.get(1));
		Assert.assertEquals(BaseTest.getText(BaseTest.findElement("xpath", "//*[@id=\"layer_cart\"]/div[@class='clearfix']//span[@id='layer_cart_product_attributes']")).split(",")[1].trim(), productDetails.get(2));
		Assert.assertEquals(BaseTest.getText(BaseTest.findElement("xpath", "//*[@id=\"layer_cart\"]/div[@class='clearfix']//span[@id='layer_cart_product_quantity']")), productDetails.get(3));
		}
	
	public  void clickProceedToCheckout() {
		BaseTest.clickWithJs(BaseTest.findElement("xpath", "//*[@id=\"layer_cart\"]/div[@class='clearfix']//a[@title='Proceed to checkout']"));
	}
	
}
