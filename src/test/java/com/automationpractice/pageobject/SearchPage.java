package com.automationpractice.pageobject;


import java.util.ArrayList;
import java.util.List;

import org.jsoup.Connection.Base;
//import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.automationpractice.utilities.BaseTest;
import com.automationpractice.utilities.ExtentRepoertsUtills;

public class SearchPage {



	public  List<WebElement> getItemsColorContainerList() {
		return BaseTest.findElements("xpath", "//ul[@class='product_list grid row']//div[@class='color-list-container']/ul");
	}

	public  List<WebElement> getProductList() {
		return BaseTest.findElements("xpath", "//ul[@class='product_list grid row']//li[starts-with(@class,'ajax_block_product ')]//h5/a[@class='product-name']");
	}


	public List<String> selectMulticolouredItem(List<String> existingProductDetails) { 
		List<String> productDetails=new ArrayList<String>();
		boolean flag=true;
		while(flag){
			List<WebElement> AllSearchedItems=BaseTest.findElements("xpath", "//div[@id='center_column']//ul[@class='product_list grid row']/li");
			int gridIndex=BaseTest.getRandomIntegerFrom0ToMaxRange(AllSearchedItems.size());
			WebElement NameofSearchedItem=BaseTest.findElement("xpath","//div[@id='center_column']//ul[@class='product_list grid row']/li["+(gridIndex+1)+"]//div[@class='right-block']//h5[@itemprop='name']");
			System.out.println(NameofSearchedItem.getText());
			List<WebElement> ColoursofSearchedItem=BaseTest.findElements("xpath", "//div[@id='center_column']//ul[@class='product_list grid row']/li["+(gridIndex+1)+"]//div[@class='right-block']//div[@class='color-list-container']/ul/li/a");
			if(ColoursofSearchedItem.size()>1){
				int colourIndex=BaseTest.getRandomIntegerFrom0ToMaxRange(ColoursofSearchedItem.size());
				String link=BaseTest.getAttribute(ColoursofSearchedItem.get(colourIndex), "href");
				String colour=link.split("/color-")[1]; 
				if(existingProductDetails.isEmpty()) {
					productDetails.add(BaseTest.getText(NameofSearchedItem));				
					productDetails.add(colour);
					BaseTest.clickWithJs(ColoursofSearchedItem.get(colourIndex));
					flag=false;
					break;	
				}else {
					boolean flag1=false;
					for (String s : existingProductDetails) {
						if(s.equals(BaseTest.getText(NameofSearchedItem)+"-"+colour)) {
							flag1=true;
							break;						
						}
					}
					if(!flag1) {
					productDetails.add(BaseTest.getText(NameofSearchedItem));				
					productDetails.add(colour);
					BaseTest.clickWithJs(ColoursofSearchedItem.get(colourIndex));
					flag=false;
					break;
					}
				}
			}
			
		}
		System.out.println(productDetails);
		return productDetails;
	}


	public  String getResultsCount() {
		WebElement resultCount=BaseTest.findElement("xpath", "//div[@id='center_column']//span[@class='heading-counter']");
		return resultCount.getText().split(" ")[0];		
	}

	public  void verifyResults(String searchItem) {
		String resultcount=getResultsCount();
		if(resultcount.equals("0"))
			verifyNoResultsMessage(searchItem);
		else
			verifyResultsMessage(searchItem);
	}

	public  void verifyResultsMessage(String searchItem) {
		WebElement resultsMessage=BaseTest.findElement("xpath", "//span[@class='lighter']");
		String itemName=resultsMessage.getText();
		String resultItem=itemName.substring(1, itemName.length()-1);
		ExtentRepoertsUtills.MyExtentReportsStatus("SearchedItemsDescription","ItemsFound", "Pass");
		Assert.assertEquals(resultItem,searchItem.toUpperCase()); 
	}

	public  void verifyNoResultsMessage(String searchItem) {
		WebElement noResultsBanner=BaseTest.findElement("xpath", "//div[@id='center_column']//p[@class='alert alert-warning']");
		String noResultText=noResultsBanner.getText();
		ExtentRepoertsUtills.MyExtentReportsStatus("SearchedItemsDescription","No ItemsFound", "Pass");
		Assert.assertEquals(noResultText,"No results were found for your search \""+searchItem+"\""); 
		
	}
}
